<?php
require_once("../setup.php");
AuthHelper::sessionStart ();

require_once (SERVER_AUTH_DIR . '/3rdparty/php-graph-sdk-5.x/src/Facebook/autoload.php');

if (AuthHelper::isAuthenticated ()) {
   redirectToSavedContext();
}

try {
   $facebook = new Facebook\Facebook([
      'app_id' => FACEBOOK_API_ID,
      'app_secret' => FACEBOOK_API_SECRET,
      'default_graph_version' => 'v3.0']);

   $helper = $facebook->getRedirectLoginHelper();
   $loginUrl = $helper->getLoginUrl(FACEBOOK_AUTH_REDIRECT_URL, ['public_profile', 'email']);
} catch(Exception $ex) {
   logErrorAndRedirect (
         "Exception during Facebook authentication, not authenticated, start from scratch " .
                print_r ( $ex, true ) );
}
header("Location: $loginUrl");
?>