<?php
require_once ("../setup.php");
AuthHelper::sessionStart ();

require_once (SERVER_AUTH_DIR . '/3rdparty/php-graph-sdk-5.x/src/Facebook/autoload.php');

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;

$globalLogger->debug ( " facebookAuthCallBack.php start" );
$globalLogger->debug ( " " . print_r ( $_GET, true ) );

try {
   $fb = new Facebook\Facebook([
      'app_id' => FACEBOOK_API_ID,
      'app_secret' => FACEBOOK_API_SECRET,
      'default_graph_version' => 'v3.0']);

   $helper = $fb->getRedirectLoginHelper();
   $globalLogger->debug ( " " . print_r ( $_GET, true ) );
   $accessToken = $helper->getAccessToken();
   $globalLogger->debug ( " " . $accessToken );
   $fb->setDefaultAccessToken($accessToken);
   $globalLogger->debug ( " next get" );
   $profileRrequest = $fb->get('/me?fields=name,first_name,last_name,email');
   $facebookUser = $profileRrequest ->getGraphNode()->asArray();

   $globalLogger->info ( " Facebook gave us facebookUser '" . print_r ( $facebookUser, true ) . "'" );

   if (! $facebookUser || ! $facebookUser["email"]) {
      logErrorAndRedirect (
            "No facebook user  or not verified after Facebook authentication, not authenticated, start from scratch " .
                   print_r ( $ex, true ), 0 );
   }

   $userInfo = new UserInfo ( $facebookUser["first_name"], $facebookUser["last_name"], $facebookUser["email"],
         $facebookUser["email"] );

   AuthHelper::authenticateUserInfoAndRedirect ( $userInfo );

} catch(Facebook\Exceptions\FacebookResponseException $ex1) {
   logErrorAndRedirect (
         "FacebookResponseException during Facebook authentication, not authenticated, start from scratch " .
                print_r ( $ex1, true ) );
} catch(Facebook\Exceptions\FacebookSDKException $ex2) {
   logErrorAndRedirect (
         "FacebookSDKException during Facebook authentication, not authenticated, start from scratch " .
                print_r ( $ex2, true ) );
}
?>