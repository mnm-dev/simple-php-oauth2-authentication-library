<?php
class DbHelper {


   public static function getDbConnection() {
      global $globalLogger;
      
      $globalLogger->debug ( "connecting to DB." );
      $mysqli = new mysqli ( AUTH_DATABASE_HOST, AUTH_DATABASE_USER, AUTH_DATABASE_PWD, AUTH_DATABASE_NAME );
      if ($mysqli->connect_errno) {
         logError ( "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error );
         exit ( - 5 );
      }
      return $mysqli;
   }
   
   // search for the passed hash
   public static function findUserHash($mysqli, $hash) {
      global $globalLogger;
      
      $globalLogger->debug ( "findUserHash: preparing statement." );
      if (! ($stmt = $mysqli->prepare ( "select 1 from user_hashes where lower(user_hash) = lower(?)" ))) {
         logError ( "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error );
         exit ( - 5 );
      }
      
      if (! ($stmt->bind_param ( "s", $hash ))) {
         logError ( "Binding parameters failed: (" . ($stmt ? $stmt->errno : "null") . ") " . $mysqli->error );
         exit ( - 5 );
      }
      
      if (! ($stmt->execute ())) {
         logError ( "execute statement failed: (" . ($stmt ? $stmt->errno : "null") . ") " . $mysqli->error );
         exit ( - 5 );
      }
      
      $res = $stmt->get_result ();
      
      if ($row = $res->fetch_assoc ()) {
         $found = true;
      } else {
         $found = false;
      }
      
      $stmt->close ();
      
      $globalLogger->debug ( "findUserHash: DB stuff finished, hash found :($found)." );
      
      return $found;
   }
}
?>
