<?php
require_once ("./setup.php");
AuthHelper::sessionStart ();

$globalLogger->debug ( "start example gatekeeper" );

// see comments in required file before changing this
require_once (SERVER_AUTH_DIR . '/isAuthorized.php');

// `isAuthorized.php` will stop the execution if the user is not authorized.
// if execution is coming to these lines, the user is logged in and good to go

$realRequestPath = realpath ( SERVER_BASE_DIR . urldecode ( AuthHelper::getSavedContext () ) );
$globalLogger->debug ( " user requested access to  '" . AuthHelper::getSavedContext () . "' ($realRequestPath)" );

if (! $realRequestPath) {
   $globalLogger->info ( "Incorrect path, redirecting to main page" );
   redirectToHomePage();
}

$canProceedWithRequest = false;
foreach ( $AUTH_GATE_KEEPER_ALLOWED_CONTEXTS as $context ) {
   if (startsWith($realRequestPath, $context)) {
       $canProceedWithRequest = true;
       break;
   }
}

if (! $canProceedWithRequest) {
   $globalLogger->info ( "Incorrect path, request ($realRequestPath) is NOT under one of the allowed contexts." );
   redirectToHomePage();
}


// should rather handle this as a rewrite rule
if (is_dir($realRequestPath)) {
   if (file_exists($realRequestPath . "/index.php" )) {
      $realRequestPath=$realRequestPath . "/index.php";
   } else if (file_exists($realRequestPath . "/index.html" )) {
      $realRequestPath=$realRequestPath . "/index.html";
   }
}


if (strtolower ( substr ( $realRequestPath, - 4 ) ) == ".php") {
   $globalLogger->info ( "require_once '$realRequestPath'" );
   // require_once prevent redirect loops for request for this file
   require_once ($realRequestPath);
} else {
   $globalLogger->info ( "readfile '$realRequestPath'" );
   readfile ( $realRequestPath );
}

?>