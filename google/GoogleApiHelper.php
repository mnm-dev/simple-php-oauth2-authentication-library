<?php
class GoogleApiHelper {
   private static $googleClient = null;


   public static function getClient() {
      if (! GoogleApiHelper::$googleClient) {
         GoogleApiHelper::initialise ();
      }
      return GoogleApiHelper::$googleClient;
   }


   public static function initialise() {
      GoogleApiHelper::$googleClient = new Google_Client ();
      GoogleApiHelper::$googleClient->setAuthConfigFile ( GOOGLE_API_CONFIG_JSON_PATH );
      GoogleApiHelper::$googleClient->addScope ( Google_Service_Oauth2::USERINFO_EMAIL );
      GoogleApiHelper::$googleClient->setRedirectUri ( GOOGLE_AUTH_REDIRECT_URL );
   }


   public static function getGoogleAuthUrl() {
      $authUrl = "";
      try {
         $authUrl = GoogleApiHelper::getClient ()->createAuthUrl ();
      } catch ( Exception $e ) {
         logErrorAndRedirect (
               "Error creating authentication URL." . "error : '" . $e->getMessage () . "'." );
      }
      return $authUrl;
   }


   public static function attemptGoogleAuthentication() {
      global $globalLogger;
      try {
         if (isset ( $_GET ['code'] )) {
            $globalLogger->debug ( " google code parameter is '" . $_GET ['code'] . "'." );
            $result = GoogleApiHelper::getClient ()->authenticate ( $_GET ['code'] );
            $globalLogger->debug ( " authenticate result  is '" . print_r($result, true) . "'." );
            $_SESSION ['login_access_token'] = $result;
         } else {
            logError ( " google code parameter is not set.", 0 );
         }
      } catch ( Exception $e ) {
         logError ( " error during authentication : '" . $e->getMessage () . "'." );
      }
   }


   public static function isGoogleAuthenticated() {
      if (isset ( $_SESSION ['login_access_token'] )) {
         return $_SESSION ['login_access_token'] != false;
      } else {
         return false;
      }
   }


   public static function getUserInfo() {
      $service = new Google_Service_Oauth2 ( GoogleApiHelper::getClient () );
      $googleUserInfo = $service->userinfo_v2_me->get ();
      
      $userInfo = new UserInfo ( $googleUserInfo->getGivenName (), $googleUserInfo->getFamilyName (), 
            $googleUserInfo->getEmail (), $googleUserInfo->getEmail () );

      return $userInfo;
   }
}

?>