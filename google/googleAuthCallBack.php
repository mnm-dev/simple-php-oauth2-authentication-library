<?php
require_once ("../setup.php");
AuthHelper::sessionStart ();

require_once (SERVER_AUTH_DIR . '/3rdparty/google-api-php-client-2.2.1/vendor/autoload.php');
require_once (SERVER_AUTH_DIR . "/google/GoogleApiHelper.php");

GoogleApiHelper::attemptGoogleAuthentication();

if (! GoogleApiHelper::isGoogleAuthenticated() ) {
   logErrorAndRedirect(" google authentication failed, not authenticated, start from scratch");
}

$userInfo=GoogleApiHelper::getUserInfo();

$globalLogger->info(" google gave us user '" . print_r($userInfo, true) . "'");

AuthHelper::authenticateUserInfoAndRedirect($userInfo);

?>
