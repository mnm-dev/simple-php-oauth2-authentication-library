<?php
require_once ("../setup.php");
AuthHelper::sessionStart ();

require_once (SERVER_AUTH_DIR . '/3rdparty/google-api-php-client-2.2.1/vendor/autoload.php');
require_once (SERVER_AUTH_DIR . "/google/GoogleApiHelper.php");

if (AuthHelper::isAuthenticated ()) {
   redirectToSavedContext ();
}

$googleLoginUrl = GoogleApiHelper::getGoogleAuthUrl ();
header ( "Location: " . $googleLoginUrl );

?>