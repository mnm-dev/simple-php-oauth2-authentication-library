<?php
class AuthHelper {


   public static function saveCurrentContext() {
      global $globalLogger;
      
      if (! array_key_exists ( "REQUEST_URI", $_SERVER )) {
         $message = "No REQUEST_URI ! The authenication flow depends on the redirect to gate keeper pages.";
         logError ( $message );
      }
      
      $targetUrl = filter_input ( INPUT_SERVER, "REQUEST_URI", FILTER_SANITIZE_URL );
      $globalLogger->info ( "targetUrl after sanitization: '$targetUrl'" );
      
      $_SESSION ["REQUEST_URI"] = $targetUrl;
   }


   public static function getSavedContext() {
      if (! array_key_exists ( "REQUEST_URI", $_SESSION )) {
         return false;
      } else {
         return $_SESSION ["REQUEST_URI"];
      }
   }


   public static function getUserInfo() {
      if (! isset ( $_SESSION ["userInfo"] )) {
         return false;
      } else {
         return $_SESSION ["userInfo"];
      }
   }


   public static function isAuthenticated() {
      global $globalLogger;
      $userInfo = AuthHelper::getUserInfo ();
      $globalLogger->info ( "isAuthenticated : '" . print_r ( $userInfo, true ) . "'" );
      return $userInfo != false;
   }


   public static function sessionStart() {
      AuthHelper::setSessionExpiry ();
      session_name ( AUTH_COOKIE_NAME );
      session_start ();
   }


   public static function findUserInWhiteList($userInfo) {
      $mysqli = DbHelper::getDbConnection ();
      $userHasWasFound = DbHelper::findUserHash ( $mysqli, $userInfo->userHash () );
      return $userHasWasFound;
   }


   public static function authenticateUserInfoAndRedirect($userInfo) {
      global $globalLogger;
      
      if (! array_key_exists ( "REQUEST_URI", $_SESSION )) {
         logError (
               "user does not have a REDIRECT URI and maybe did not follow authenication flow.' " .
                      print_r ( $userInfo, true ) . "'" );
         exit ( - 2 );
      }
      
      if (AuthHelper::findUserInWhiteList ( $userInfo )) {
         
         AuthHelper::setAsAuthenticatedAndRedirect ( $userInfo );
      } else {
         $globalLogger->warn ( " user  '" . print_r ( $userInfo, true ) . "' is not whitelisted." );
         header ( "Location: " . UNKNOWN_USER_PAGE );
      }
   }


   public static function setAsAuthenticatedAndRedirect($userInfo, $redirectPath = null) {
      global $globalLogger;

      AuthHelper::setSessionExpiry ();

      // new session to help against stolen sessions
      session_regenerate_id ();
      
      $_SESSION ["userInfo"] = $userInfo;
      
      if ($redirectPath) {
         $_SESSION ["REQUEST_URI"] = $redirectPath;
      }
      
      $globalLogger->info (
            " user is authenticated: '" . print_r ( $userInfo, true ) . "', redirecting to '$_SESSION[REQUEST_URI]'" );
      header ( "Location: " . $_SESSION ["REQUEST_URI"] );
   }


   public static function hashIt($string) {
      return hash ( "sha256", $string );
   }


   public static function setSessionExpiry() {

      /*
       * We dont want to handle old sessions, so we simply only use sessions
       *
       * In case someone wants to use longer sessions:
       */

      // keep sessions for four weeks
      // ini_set ( 'session.cookie_lifetime', 60 * 60 * 24 * 28 );
      // ini_set ( 'session.gc_maxlifetime', 60 * 60 * 24 * 28 );
      // ini_set ( 'session.save_path', /tmp/you-have-to-set-this-folder-up );
   }
}
?>
