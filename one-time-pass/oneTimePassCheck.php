<?php
require_once ("../setup.php");
AuthHelper::sessionStart ();

require_once (SERVER_AUTH_DIR . "/one-time-pass/OneTimePassDbHelper.php");
require_once (SERVER_AUTH_DIR . "/one-time-pass/OneTimePassRequestLimit.php");

function validateHashParameter() {
   global $globalLogger;
   $hash = filter_input ( INPUT_GET, 'hash', FILTER_SANITIZE_SPECIAL_CHARS, 
         FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH );
   $globalLogger->debug ( "oneTimePassCheck.php : '$hash'" );
   
   if (empty ( $hash )) {
      $globalLogger->info ( "oneTimePassCheck: 'hash' not set or no correct ($_POST[hash])." );
      return false;
   }
   
   return $hash;
}

if (! ($oneTimePassToken = validateHashParameter ())) {
   redirectToHomePage ();
}

$mysqli = DbHelper::getDbConnection ();

if (OneTimePassRequestLimit::overLimit ( $mysqli )) {
   redirectToHomePage ();
}

$oneTimePassInfo = OneTimePassDbHelper::checkOneTimePass ( $mysqli, $oneTimePassToken );
if (empty ( $oneTimePassInfo )) {
   redirectToHomePage ();
}

$userInfo = new UserInfo( "","","","",$oneTimePassInfo ["user_hash"] );

AuthHelper::setAsAuthenticatedAndRedirect ( $userInfo, $oneTimePassInfo ["request_url"] );

?>