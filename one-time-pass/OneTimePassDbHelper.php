<?php
class OneTimePassDbHelper {


   public static function getNumberOfRecentOneTimePassRequests($mysqli, $recentMinutes, $userHash) {
      global $globalLogger;
      
      $stmt = $mysqli->prepare ( 
            "select count(0) from one_time_pass_requests where request_time > CURRENT_TIMESTAMP - INTERVAL ? MINUTE and lower(user_hash)=lower(?)" );
      $stmt && $bindResult = $stmt->bind_param ( "ds", $recentMinutes, $userHash );
      
      if (empty ( $bindResult )) {
         logError ( "Binding parameters failed: (" . ($stmt ? $stmt->errno : "null") . ") " . $mysqli->error );
         exit ( - 1 );
      }
      
      $globalLogger->debug ( "getNumberOfRecentOneTimePassRequests executing statement." );
      if (! ($stmt->execute ())) {
         logError ( "execute statement failed: (" . ($stmt ? $stmt->errno : "null") . ") " . $mysqli->error );
         exit ( - 1 );
      }
      
      $globalLogger->debug ( "getting result." );
      $res = $stmt->get_result ();
      
      $globalLogger->debug ( "getting data." );
      if ($row = $res->fetch_array ( MYSQLI_NUM )) {
         $count = $row [0];
      } else {
         $count = 0;
      }
      
      $globalLogger->info ( "found $count in the last $recentMinutes minutes for email '$userHash', closing stmt" );
      $stmt->close ();
      
      return $count;
   }


   public static function newOneTimePassRequest($mysqli, $userHash, $redirectUrl) {
      global $globalLogger;

      if (empty ( $userHash )) {
         logError ( "newOneTimePassRequest called with empty forEmail parameter." );
         exit ( - 1 );
      }

      $hash = hash ( "sha256", 
            uniqid () . "Token." . $userHash . print_r ( $_SERVER, true ) . print_r ( $_COOKIE, true ) );

      $globalLogger->debug ( "newOneTimePassRequest preparing statement." );
      if (! ($stmt = $mysqli->prepare (
            "INSERT INTO one_time_pass_requests (user_hash, otp_hash, request_url) VALUES (?, ?, ?);" ))) {
         logError ( "newOneTimePassRequest: Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error );
         exit ( - 1 );
      }

      $globalLogger->debug ( "newOneTimePassRequest: binding to statement." );
      if (! ($stmt->bind_param ( "sss", $userHash, $hash, $redirectUrl ))) {
         logError (
               "newOneTimePassRequest: Binding parameters failed: (" . ($stmt ? $stmt->errno : "null") . ") " .
                      $mysqli->error );
         exit ( - 1 );
      }

      $globalLogger->debug ( "newOneTimePassRequest: executing statement." );
      if (! ($stmt->execute ())) {
         logError (
               "newOneTimePassRequest: execute statement failed: (" . ($stmt ? $stmt->errno : "null") . ") " .
                      $mysqli->error );
         exit ( - 1 );
      }

      $globalLogger->debug ( "newOneTimePassRequest: closing statement." );
      $stmt->close ();

      return $hash;
   }


   public static function checkOneTimePass($mysqli, $oneTimePassToken) {
      if (empty ( $oneTimePassToken )) {
         logError ( "checkOneTimePass called with empty hash parameter." );
         exit ( - 1 );
      }
      
      $oneTimePassInfo = OneTimePassDbHelper::searchDbForOneTimePass ( $mysqli, $oneTimePassToken );
      
      if ($oneTimePassInfo) {
         OneTimePassDbHelper::markOneTimePassAsUsed ( $mysqli, $oneTimePassToken );
      }
      
      OneTimePassDbHelper::deletePassesOlderThanTwoDays ( $mysqli );
      
      return $oneTimePassInfo;
   }


   public static function deletePassesOlderThanTwoDays($mysqli) {
      global $globalLogger;
      
      $globalLogger->debug ( "deletePassesOlderThanTwoDays: preparing statement." );
      if (! ($stmt = $mysqli->prepare ( 
            "DELETE FROM one_time_pass_requests WHERE request_time < CURRENT_TIMESTAMP - INTERVAL 7 DAY" ))) {
         logError ( "deletePassesOlderThanTwoDays Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error );
         exit ( - 1 );
      }
      if (! ($stmt->execute ())) {
         logError (
               "deletePassesOlderThanTwoDays: execute statement failed: (" . ($stmt ? $stmt->errno : "null") . ") " .
                      $mysqli->error );
         exit ( - 1 );
      }
      $stmt->close ();
      $globalLogger->debug ( "deletePassesOlderThanTwoDays: closed statement." );
   }


   private static function searchDbForOneTimePass($mysqli, $oneTimePassToken) {
      global $globalLogger;
      
      $globalLogger->debug ( "searchDbForOneTimePass: preparing statement." );
      if (! ($stmt = $mysqli->prepare ( "select * from one_time_pass_requests where otp_hash = ?" ))) {
         logError ( "searchDbForOneTimePass: Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error );
         exit ( - 1 );
      }
      
      $globalLogger->debug ( "binding to statement." );
      if (! ($stmt->bind_param ( "s", $oneTimePassToken ))) {
         logError (
               "searchDbForOneTimePass: Binding parameters failed: (" . ($stmt ? $stmt->errno : "null") . ") " .
                      $mysqli->error );
         exit ( - 1 );
      }
      
      $globalLogger->debug ( "executing statement." );
      if (! ($stmt->execute ())) {
         logError (
               "searchDbForOneTimePass: execute statement failed: (" . ($stmt ? $stmt->errno : "null") . ") " .
                      $mysqli->error );
         exit ( - 1 );
      }
      
      $globalLogger->debug ( "searchDbForOneTimePass: getting result." );
      $res = $stmt->get_result ();
      
      $globalLogger->debug ( "searchDbForOneTimePass: getting data." );
      $row = false;
      if ($row = $res->fetch_assoc ()) {
         $oneTimePassToken = $row ["otp_hash"];
         if (empty ( $row ["used"] )) {
            $globalLogger->info ( "searchDbForOneTimePass: hash '$oneTimePassToken' found, has not been used yet" );
         } else {
            $globalLogger->info ( "searchDbForOneTimePass: hash '$oneTimePassToken' found, but was already used '$row[used])'." );
            $row = false;
         }
      } else {
         $globalLogger->info ( "searchDbForOneTimePass: hash not found." );
      }
      
      $globalLogger->debug ( "searchDbForOneTimePass: closing stmt" );
      $stmt->close ();
      
      return $row;
   }


   private static function markOneTimePassAsUsed($mysqli, $oneTimePassToken) {
      global $globalLogger;
      $globalLogger->debug ( "markHashAsUsed: updating hash used date" );
      if (! ($stmt = $mysqli->prepare ( "UPDATE one_time_pass_requests SET used=CURRENT_TIMESTAMP where otp_hash = ?" ))) {
         logError ( "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error );
         exit ( - 1 );
      }
      
      $globalLogger->debug ( "markHashAsUsed: binding to statement." );
      if (! ($stmt->bind_param ( "s", $oneTimePassToken ))) {
         logError ( "Binding parameters failed: (" . ($stmt ? $stmt->errno : "null") . ") " . $mysqli->error );
         exit ( - 1 );
      }
      
      $globalLogger->debug ( "markHashAsUsed: executing statement." );
      if (! ($stmt->execute ())) {
         logError ( "execute statement failed: (" . ($stmt ? $stmt->errno : "null") . ") " . $mysqli->error );
         exit ( - 1 );
      }
      $globalLogger->info ( "markHashAsUsed: closing stmt" );
      $stmt->close ();
   }
}

?>
