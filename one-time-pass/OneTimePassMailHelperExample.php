<?php
class OneTimePassMailHelper {


   public static function sendMail($email, $hash) {
      global $globalLogger;
      
      $callBackUrl = SERVER_URL . SERVER_AUTH_CONTEXT . "/one-time-pass/oneTimePassCheck.php?hash=$hash";
      
      $message = "
<html>
<head>
  <title>One-Time Pass</title>
</head>
<body>
  <p>Someone just requested a one-time pass for you.</p>
  <p>The below link (the pass) allows you to access the secured content in the next 48 hours.</p>
  <p>In case you dont know anything about this, ask us for more information.</p>
  <p><a href='$callBackUrl'>Access to the secured Example.com content</a></p>
</body>
</html>
";
      
      $subject = 'Example One-Time-Pass';
      
      $headers = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      
      $headers .= "To: <$email>" . "\r\n";
      $headers .= 'From: admin <admin@example.com>' . "\r\n";
      $headers .= 'Bcc: admin <admin@example.com>' . "\r\n";
      
      $globalLogger->debug ( "sending email to '$email' with callbackUrl=$callBackUrl" );
      mail ( $email, $subject, $message, $headers, "-f admin@example.com" );
   }
}

?>