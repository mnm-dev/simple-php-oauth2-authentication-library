<?php
require_once ("../setup.php");
AuthHelper::sessionStart ();

if (AuthHelper::isAuthenticated ()) {
   redirectToSavedContext();
}

$oneTimePassEmailProcessingPage = SERVER_AUTH_CONTEXT . "/one-time-pass/oneTimePassEmailForm.php";

if (! AuthHelper::getSavedContext ()) {
   $globalLogger->info (
         "'AuthHelper::getSavedContext()' returns nothing, most likely the authentication workflow has not been followed." );
   redirectToHomePage();
}

require (ONE_TIME_PASS_FORM_URL);

?>