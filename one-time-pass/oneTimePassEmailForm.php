<?php
require_once ("../setup.php");
AuthHelper::sessionStart ();

require_once (SERVER_AUTH_DIR . "/one-time-pass/OneTimePassMailHelper.php");
require_once (SERVER_AUTH_DIR . "/one-time-pass/OneTimePassDbHelper.php");
require_once (SERVER_AUTH_DIR . "/one-time-pass/OneTimePassRequestLimit.php");

function forwardToSubmissionReceivedPage() {
   header ( "Location: " . ONE_TIME_PASS_FORM_RESULT_PAGE );
   exit ( 0 );
}


function validateParameters() {
   global $globalLogger;
   $email = filter_input ( INPUT_POST, 'email', FILTER_VALIDATE_EMAIL );
   
   $globalLogger->debug ( "oneTimePassEmailForm: cleaned email: '$email'" );
   
   if (empty ( $email )) {
      $globalLogger->warn ( "oneTimePassEmailForm: 'email' not set or no correct." );
      return false;
   }
   
   if (! AuthHelper::getSavedContext ()) {
      $globalLogger->warn ( 
            "oneTimePassEmailForm: 'AuthHelper::getSavedContext()' returns nothing, most likely the authentication workflow has not been followed." );
      return false;
   }
   
   return $email;
}

if (! ($email = validateParameters ())) {
   forwardToSubmissionReceivedPage ();
}

$mysqli = DbHelper::getDbConnection ();

if (OneTimePassRequestLimit::overLimit ( $mysqli )) {
   forwardToSubmissionReceivedPage ();
}

$emailHash = AuthHelper::hashIt ( $email );

if (! DbHelper::findUserHash ( $mysqli, $emailHash )) {
   $globalLogger->warn ( "oneTimePassEmailForm: '$email' is not on our white list." );
   forwardToSubmissionReceivedPage ();
}

$specificEmailRequests = OneTimePassDbHelper::getNumberOfRecentOneTimePassRequests ( $mysqli, 3, $emailHash );
if ($specificEmailRequests > 2) {
   $globalLogger->warn ( 
         "There were '$specificEmailRequests' one-time pass requests for '$email' in the last 3 minutes and only 2 were allowed." );
   forwardToSubmissionReceivedPage ();
}

$oneTimePassToken = OneTimePassDbHelper::newOneTimePassRequest ( $mysqli, $emailHash, AuthHelper::getSavedContext () );

OneTimePassMailHelper::sendMail ( $email, $oneTimePassToken );
forwardToSubmissionReceivedPage ();

?>