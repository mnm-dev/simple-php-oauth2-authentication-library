<?php
class OneTimePassRequestLimit {


   public static function overLimit($mysqli) {
      global $globalLogger;
      
      OneTimePassRequestLimit::saveNewOneTimePassInteraction ( $mysqli );
      OneTimePassRequestLimit::deleteHistoricOneTimePassInteraction ( $mysqli );

      $requests = OneTimePassRequestLimit::getNumberOfRecentInteractions ( $mysqli, 5 );
      if ($requests > 10) {
         $globalLogger->warn ( "OneTimePassRequestLimit: Over the limit of 10 requests in 5 minutes. ($requests)" );
         return true;
      }

      $requests = OneTimePassRequestLimit::getNumberOfRecentInteractions ( $mysqli, 1440 );
      if ($requests > 100) {
         logError ( "OneTimePassRequestLimit: Over the limit of 100 requests in 1 day. ($requests)" );
         return true;
      }

      $requests = OneTimePassRequestLimit::getNumberOfRecentInteractions ( $mysqli, 10080 );
      if ($requests > 300) {
         logError ( "OneTimePassRequestLimit: Over the limit of 300 requests in 7 days. ($requests)" );
         return true;
      }
      
      return false;
   }


   private static function deleteHistoricOneTimePassInteraction($mysqli) {
      if (! ($stmt = $mysqli->prepare (
            "DELETE FROM one_time_pass_interactions WHERE request_time < CURRENT_TIMESTAMP - INTERVAL 2 DAY;" ))) {
         logError ( "deletePassesOlderThanTwoDays Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error );
         exit ( - 1 );
      }
      if (! ($stmt->execute ())) {
         logError (
               "deletePassesOlderThanTwoDays: execute statement failed: (" . ($stmt ? $stmt->errno : "null") . ") " .
                      $mysqli->error );
         exit ( - 1 );
      }
      $stmt->close ();
   }


   private static function saveNewOneTimePassInteraction($mysqli) {
      if (! ($stmt = $mysqli->prepare ( "INSERT INTO one_time_pass_interactions VALUES ();" ))) {
         logError ( "saveNewOneTimePassInteraction Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error );
         exit ( - 1 );
      }
      if (! ($stmt->execute ())) {
         logError (
               "saveNewOneTimePassInteraction: execute statement failed: (" . ($stmt ? $stmt->errno : "null") . ") " .
                      $mysqli->error );
         exit ( - 1 );
      }
      $stmt->close ();
   }


   private static function getNumberOfRecentInteractions($mysqli, $recentMinutes) {
      $stmt = $mysqli->prepare (
            "select count(0) from one_time_pass_interactions where request_time > CURRENT_TIMESTAMP - INTERVAL ? MINUTE" );
      $stmt && $bindResult = $stmt->bind_param ( "d", $recentMinutes );
      
      if (empty ( $bindResult )) {
         logError ( "Binding parameters failed: (" . ($stmt ? $stmt->errno : "null") . ") " . $mysqli->error );
         exit ( - 1 );
      }
      
      if (! ($stmt->execute ())) {
         logError ( "execute statement failed: (" . ($stmt ? $stmt->errno : "null") . ") " . $mysqli->error );
         exit ( - 1 );
      }
      
      $res = $stmt->get_result ();
      if ($row = $res->fetch_array ( MYSQLI_NUM )) {
         $count = $row [0];
      } else {
         $count = 0;
      }
      $stmt->close ();
      return $count;
   }
}

?>