<?php
require_once (dirname ( __FILE__ ) . "/config/auth-config.php");

require_once (SERVER_AUTH_DIR . '/3rdparty/apache-log4php-2.3.0/Logger.php');

// Tell log4php to use our configuration file.
Logger::configure ( SERVER_AUTH_DIR . '/config/log4php-config.xml' );
$globalLogger = Logger::getLogger ( "all" );

require_once (SERVER_AUTH_DIR . "/auth-setup.php");

?>