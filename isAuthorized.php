<?php

// the require below includes Wordpress files and MUST not be called from a function
// otherwise :
// PHP Fatal error: Call to a member function set_prefix() on null in .../wordpress/wp-includes/ms-settings.php on line 196


AuthHelper::saveCurrentContext();

if (! AuthHelper::isAuthenticated ()) {
   
   $globalLogger->debug ( "isAuthorized : false" );
   
   $googleAuthenticationStartPage = SERVER_AUTH_CONTEXT . "/google/googleAuthRedirect.php";
   $facebookAuthenticationStartPage = SERVER_AUTH_CONTEXT . "/facebook/facebookAuthRedirect.php";
   $oneTimePassRedirectPage = SERVER_AUTH_CONTEXT . "/one-time-pass/oneTimePassFormRedirect.php";

   // The login information page is outside the gallery project so that it can be themed for the web site,
   // check out examples/login-example.php
   // we set the URLs for each authentication type before including the page
   // the login page does not require any "requires" apart from what it itself requires for its rendering
   //
   // relevant global vars:
   //
   // $googleAuthenticationStartPage      the URL to the internal page that will set up the
   //      correct Google Open ID Connect authentication URL and then redirect to it
   //
   // $facebookAuthenticationStartPage    the URL to the internal page that will set up the
   //      correct Facebook authentication URL and then redirect to it 
   //
   // $oneTimePassEmailFormPage           the URL of the $oneTimePassEmailFormPage
   //

   require (LOGIN_PAGE_PHP_PATH);
   
   exit ( 0 );
}
$globalLogger->debug ( "isAuthorized : true" );
?>