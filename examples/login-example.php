<?php
if (! isset ( $googleAuthenticationStartPage ) || ! isset ( $facebookAuthenticationStartPage ) ||
       ! isset ( $oneTimePassRedirectPage )) {

   logErrorAndRedirect (
         "googleAuthenticationStartPage or facebookAuthenticationStartPage  or oneTimePassRedirectPage is not set. " .
          __FILE__ . " should not be called directly. " .
          "Instead it is called from gate keepers that will make sure the redirection path is setup correctly" );
}

// require_once(SERVER_BASE_DIR . "/top.php");
?>

<html>
<body>
   <header>
      <h1>Access to Secure Content</h1>
   </header>
   <div>
      To be able to access our secured content you have to:

      <ul>
         <li>Be known to us:
            <ul>
               <li>You have to have a gmail email address that we know</li>
               <li>You have a Facebook account that has a primary email address that we know.</li>
            </ul>
         </li>
         <li>Be known to us well enough for us to think nothing of showing you pictures of Markus cleaning a plate.</li>
         <li>Authenticate via Gmail or Facebook and allow this web-site to see your email address</li>
         <li>If neither Facebook nor Google authentication is up your alley, we can sent a one-time-pass to an email
            that we know.</li>
      </ul>

      Please choose your preferred login:
      <ul>
         <li><a href="<?php echo $googleAuthenticationStartPage ?>">Login via gmail</a></li>
         <li><a href="<?php echo $facebookAuthenticationStartPage ?>">Login via Facebook</a></li>
         <li><a href="<?php echo $oneTimePassRedirectPage ?>">Login via an one-time pass</a></li>
      </ul>
   </div>
</body>
</html>

<?PHP
// require_once(SERVER_BASE_DIR . "/bottom.php");
?>
