<?php
if (empty ( $oneTimePassEmailProcessingPage )) {
   logErrorAndRedirect (
         "oneTimePassEmailProcessingPage is not set. " . __FILE__ . " should not be called directly. " .
                "Instead it is called from gate keepers that will make sure the redirection path is correct" );
}

// require_once(SERVER_BASE_DIR . "/top.php");
?>


<html>
<body>

   <header>
      <h1 class="entry-title">One-Time Pass Request to Secured Content</h1>
   </header>
   <div class="entry-content hentry">

      <div style="padding: 10px 0px;">Please enter you email and submit it. We will match it with all the emails that we
         know off.</div>

      <div style="padding: 10px 0px;">
         Depending on how this goes, we will send you a one-time pass link that will allow you to access the secured
         content.<br> You can use the link for yourself or you can forward it to someone else, but the token will only
         be valid for one use in one session. Next time you will have to get another one. The one-time pass link is only
         usable in the next 48h.
      </div>

      <div style="padding: 10px 0px;">
         <form action="<?php echo $oneTimePassEmailProcessingPage; ?>" method="post">
            <fieldset>
               <legend>What email address would you like to send the one-time pass to?</legend>
               <span style="font-style: italic;">This email has to be known to us.</span>
               <div style="padding: 20px;">
                  <label for="email">Email:</label><input id="email" name="email" type="email" />
                  <button style="line-height: 1.2em" type="submit">Submit</button>
               </div>
            </fieldset>
         </form>
      </div>
   </div>
</body>
</html>

<?PHP
// require_once(SERVER_BASE_DIR . "/bottom.php");
?>
