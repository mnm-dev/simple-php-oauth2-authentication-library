<?php
require_once (SERVER_AUTH_DIR . "/UserInfo.php");
require_once (SERVER_AUTH_DIR . "/AuthHelper.php");
require_once (SERVER_AUTH_DIR . '/DbHelper.php');


function startsWith($haystack, $needle) {
   return $needle !== false && strpos ( $haystack, $needle ) === 0;
}


function redirectToHomePage() {
   header ( "Location: " . HOME_PAGE_CONTEXT );
   exit ( 0 );
}


function redirectToSavedContext() {
   if (! ($redirectTo = AuthHelper::getSavedContext ())) {
      $redirectTo = HOME_PAGE_CONTEXT;
   }
   header ( "Location: " . $redirectTo );
   exit ( 0 );
}


function logError($msg) {
   global $globalLogger;
   error_log ( "Error! See log4php authentication log file for more information.", 0 );
   $globalLogger->error ( $msg );
}


function logErrorAndRedirect($message) {
   logError ( $message );
   redirectToSavedContext ();
}

?>