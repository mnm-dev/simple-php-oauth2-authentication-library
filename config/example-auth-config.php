<?php

// copy this file to auth-config.php and then adapt it to your environment

// consider moving these settings into a site config file and including them in this
// consider moving all sensitive information into external files that are not served by the web server
// e.g. /etc/site-configuration/

// none of these configuration files must have whitespace before < ?php or trailing after ? >
define ( 'SERVER_HOST_NAME', 'www.example.com' );
define ( "SERVER_BASE_DIR", '/srv/www/htdocs' );
define ( 'SCHEME_FREE_SERVER_URL', '//' . SERVER_HOST_NAME );
define ( 'SERVER_URL', 'https:' . SCHEME_FREE_SERVER_URL );

// Either include a file with DB Connection details (eg. SITE_DATABASE_...)

require_once ("/etc/site-configuration/db_config.php");

// or define them directly in this file
define ( 'AUTH_DATABASE_HOST', SITE_DATABASE_HOST );
define ( 'AUTH_DATABASE_NAME', SITE_DATABASE_NAME );
define ( 'AUTH_DATABASE_USER', SITE_DATABASE_USER );
define ( 'AUTH_DATABASE_PWD', SITE_DATABASE_PWD );

// change SERVER_AUTH_CONTEXT to the name of your Simple PHP authentication library sub-folder
define ( "SERVER_AUTH_CONTEXT", "/auth" );
define ( "AUTH_COOKIE_NAME", "example-auth-cookie" );


// This file has your API key and must not be under the servers document root
define ( "GOOGLE_API_CONFIG_JSON_PATH", "/etc/site-configuration/google_api_config.json" );

// this file should define the fasebook API key/secret :
// define ( "FACEBOOK_API_ID", "000000000000" );
// define ( "FACEBOOK_API_SECRET", "SOME_SECRET" );
require_once ('/etc/site-configuration/facebook_api_keys_config.php');

// the example gate keeper is only relaying requests with real-path starting with one of these strings (this matches hierarcheis as well as specific files):
$AUTH_GATE_KEEPER_ALLOWED_CONTEXTS = array (SERVER_BASE_DIR . "/protected",
         '/some/specific/path/to/a/file.php',SERVER_BASE_DIR . "/auth/examples" 
);

// all the definitions in this file are a result of the above configuration
require_once (dirname ( __FILE__ ) . "/general-auth-config.php");

/*
 *
 * external themed pages
 *
 * PHP pages are loaded by the app with require and HTML pages are redirected to
 *
 */

// a page informing about the different login options
define ( "LOGIN_PAGE_PHP_PATH", SERVER_AUTH_DIR . "/examples/login-example.php" );

// users that got authenticated some way, but are not known to use are redirect there:
define ( "UNKNOWN_USER_PAGE", SERVER_AUTH_CONTEXT . "/examples/unknown-user-example.html" );

// A page with a form that posts
define ( "ONE_TIME_PASS_FORM_URL", SERVER_AUTH_DIR . "/examples/one-time-token-form-example.php" );

//
define ( "ONE_TIME_PASS_FORM_RESULT_PAGE", 
      SERVER_AUTH_CONTEXT . "/examples/one-time-pass-request-submitted-example.html" );

?>