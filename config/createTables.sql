CREATE TABLE user_hashes ( user_hash varchar(130) primary key, info varchar(100) unique);

CREATE TABLE one_time_pass_requests ( request_time DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
        user_hash VARCHAR(250) NOT NULL, otp_hash VARCHAR(250) NOT NULL, 
        request_url VARCHAR(500) NOT NULL, used DATETIME);
  
CREATE TABLE one_time_pass_interactions (request_time DATETIME DEFAULT CURRENT_TIMESTAMP);

