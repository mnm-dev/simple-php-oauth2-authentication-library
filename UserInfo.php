<?php 
class UserInfo {

    private $firstName;
    private $lastname;
    private $email;
    private $userId;
    private $userHash;
    
    /**
     * when we have an userHash we use that, otherwise we hash the userId
     */
    public function __construct($firstName, $lastname, $email, $userId, $userHash=NULL)
    {
        $this->firstName = $firstName;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->userId = $userId;
        if (!$userHash) {
           $this->userHash = AuthHelper::hashIt($userId);
        } else {
           $this->userHash = $userHash;
        }
    }


   public function firstName() {
      return $this->firstName;
   }
   
   public function lastname() {
      return $this->lastname;
   }
   
   public function email() {
      return $this->email;
   }
   
   public function userId() {
      return $this->userId;
   }
   
   public function userHash() {
      return $this->userHash;
   }

}
?>