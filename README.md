# Simple PHP OAuth2 authentication library #

## Purpose ##

A simple library/setup to allow known users to authenticate to your web site via

  - Google's OpenID Connect (OAuth 2.0 for Login) using [gmail][1]
  - Facebook Connect 
  - One-Time-Password (OTP)

Uses the Google/Facebook API and their PHP implementation to integrate.
 
Not recommended for sensitive information or large-traffic site. (Written with a complete disregard of performance.)

## Simplified Flow ##

The examples use MnM.at as the website requiring authentication

### Oauth/Connect Flow ###

The example uses gmail as the Oauth2 server.

1.  An unauthenticated user accesses some secured content on MnM.at
2.  MnM.at creates a session for the user (session cookie) and stores the original request URI
3.  The user is offered to login via gmail
4.  MnM.at requests a token from gmail
5.  MnM.at redirects the user to gmail with the prepared request token 
6.  The user either is already logged-on to gmail or logs in
7.  gmail asks the user if MnM.at can be trusted with the requested information (the user's email-address)
8.  gmail redirects the user back to  MnM.at with a response token
9.  MnM.at uses the response token to request access to the user's email address
0.  MnM.at matches the email address against a list of known users 
1.  MnM.at marks the user as authenticated in the session  
2.  MnM.at redirects the user to the stored original request URI


### One-Time-Password ###

1.  An unauthenticated user accesses some secured content on MnM.at
2.  MnM.at creates a session for the user (session cookie)
3.  The user is offered to log-in via One-Time-Pass(word)
4.  The user submits an email address to MnM.at
5.  MnM.at matches the email address against a list of known users 
6.  MnM.at generates a One-Time-Pass, stores it with the original request URI 
7.  MnM.at sends an email to the known user with a link containing the One-Time-Pass
8.  The users receives the email and clicks the link.
9.  MnM.at checks the One-Time-Pass and redirects 
0.  MnM.at marks the user as authenticated in the session  
1.  MnM.at redirects the user to the stored original request URI

## Security Concerns ##

There are some security concerns with oauth2 which supposedly are addressed by gmail and Facebook.

One Time Passes can be intercepted and used.

Session management means the users 

This code is doing stupid things.   


## Background ##

This code was split off from a trial to get known users access to our website's photo galleries.

Our galleries were hidden behind basic-authentication with a very simple user-name that was also the password.
Our friends still kept forgetting the simple user-name/password combination.
While our galleries do not contain any sensitive information we prefer not to have them available to the world.



## Setup ##

Be careful not to have leading or trailing whitespace in any php file (e.g. external configurations). This would break the authentication. 

## Requirements ##

   - Apache HTTP with mos\_rewrite  and mod\_unique_id
   - php mysqli and a MySQL or Maria DB (state 2015 January)
   
### Configuration ###

   - Under the 3rdParty folder set up the 3rd part libs with : `install-libs` 
   - Setup Google account and project like shown [here][2]. 
   - save the Google app configuration json to a path that is not served by the web server
   - Setup Facebook Developer account and project like shown [here][3]. 
   - rename `config/example-auth-config.php` to `auth-config.php` and fix up your settings.
   - correct and set-up the `SESSION_SAVE_DIR`
   - create a nice log-in and unknown-user page
   - instead of a CDN you can directly use server paths by adapting the configuration
   - emailing from php has to work
   - Browse to http://yourserver/auth/example/secured/test/ 
   - Debug


### Authentication flow ###

mod_rewrite rules redirect all access to protected areas to the gate keeper pages.
E.g. `auth/examples/.htaccess` has this  rule:
   
    RewriteRule  (secured/.*) %{CONTEXT_DOCUMENT_ROOT}/auth/index.php

This redirects all acecss to content under `auth/examples/secured` to `/auth/index.php`.

The gate keepers are responsible for particular secured areas and check if the user is authenticated.
The server will set the URI of the original request as $\_SERVER['REQUEST_URI'].
The gate keeper pages add REQUEST_URI to the session and use the session value to show the content after successful authentication.

  [1]: https://developers.google.com/accounts/docs/OpenIDConnect
  [2]: https://developers.google.com/api-client-library/php/start/get_started 
  [3]: https://developers.facebook.com/docs/php/gettingstarted/4.0.0


